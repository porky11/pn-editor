use bool_utils::BoolUtils;
use femtovg::{Align, Baseline, Canvas, Color, FontId, LineJoin, Paint, Path, Renderer};
use ga2::Vector;
use pn_editor_core::{ArrowKind, CallableState, Editor, Renderer as GraphRenderer, ViewMode};
use simple_femto_text_rendering::{CursorSettings, render_text};
use text_editing::TextLine;
use vector_space::InnerSpace;

pub enum RenderFlow {
    OmniDirectional,
    Horizontal,
    Vertical,
}

pub struct NetRenderer<R: Renderer> {
    text_size: f32,
    text_width: f32,
    pub font: FontId,
    pub canvas: Canvas<R>,
    flow: RenderFlow,
    show_help: bool,
}

impl<R: Renderer> NetRenderer<R> {
    pub fn new(canvas: Canvas<R>, font: FontId) -> Self {
        Self {
            canvas,
            text_size: 16.0,
            text_width: 128.0,
            font,
            flow: RenderFlow::OmniDirectional,
            show_help: false,
        }
    }

    pub fn set_flow(&mut self, flow: RenderFlow) {
        self.flow = flow;
    }

    pub fn toggle_help(&mut self) {
        self.show_help.toggle();
    }
}

impl<R: Renderer> NetRenderer<R> {
    fn draw_cursor(
        &mut self,
        mut x: f32,
        mut y: f32,
        zoom: f32,
        text: &str,
        text_cursor: usize,
        text_paint: &Paint,
    ) {
        if let (Ok(metrics), Ok(full_metrics)) = (
            self.canvas
                .measure_text(x, y, &text[0..text_cursor], text_paint),
            self.canvas.measure_text(x, y, text, text_paint),
        ) {
            x += metrics.width();
            match text_paint.text_align() {
                Align::Left => (),
                Align::Center => x -= full_metrics.width() / 2.0,
                Align::Right => x -= full_metrics.width(),
            }
            let h = self.text_size / 2.0 / zoom;
            match text_paint.text_baseline() {
                Baseline::Top => y += h,
                Baseline::Middle => (),
                Baseline::Alphabetic => (),
                Baseline::Bottom => y -= h,
            }
            let mut path = Path::new();
            path.move_to(x, y - h);
            path.line_to(x, y + h);
            self.canvas.stroke_path(&path, text_paint);
        }
    }
}

impl<R: Renderer> GraphRenderer for NetRenderer<R> {
    fn draw_simulated(&mut self, simulated: Option<usize>) {
        let bg = 1.0 / 4.0;
        let color = Color::rgbf(bg, bg, bg);

        self.canvas
            .clear_rect(0, 0, self.canvas.width(), self.canvas.height(), color);

        let mut path = Path::new();
        let w = self.canvas.width() as f32;
        let size = w.min(self.canvas.height() as f32) / 0x10 as f32;
        let fill = 0.5;
        let fill_color = if simulated.is_some() {
            path.move_to(w - size, size / 2.0);
            path.line_to(w - size / 2.0, size * 0.75);
            path.line_to(w - size, size);
            path.close();
            Color::rgbf(fill, fill, fill - 1.0 / 4.0)
        } else {
            path.rect(size.mul_add(-0.8, w), size / 2.0, -size * 0.2, size / 2.0);
            path.rect(size.mul_add(-0.5, w), size / 2.0, -size * 0.2, size / 2.0);
            Color::rgbf(fill, fill, fill + 1.0 / 4.0)
        };
        self.canvas.stroke_path(
            &path,
            &Paint::color(Color::black())
                .with_line_width(size / 0x10 as f32)
                .with_line_join(LineJoin::Round),
        );
        self.canvas.fill_path(&path, &Paint::color(fill_color));

        if let Some(index) = simulated {
            let font_size = 0x20 as f32;
            let text_stroke = Paint::color(Color::black())
                .with_font_size(font_size)
                .with_text_baseline(Baseline::Top)
                .with_line_width(1.0);
            let text_fill = text_stroke.clone().with_color(Color::white());
            let x = size.mul_add(0.75, w);
            let y = size * 0.75;
            let text = format!("{}", index + 1);
            let _ = self.canvas.fill_text(x, y, &text, &text_fill);
            let _ = self.canvas.stroke_text(x, y, &text, &text_stroke);
        }
    }

    fn finish(&mut self, editor: &Editor) {
        let font_size = 0x20 as f32;

        let box_stroke = Paint::color(Color::black());
        let box_fill = Paint::color(Color::rgb(0xC0, 0xC0, 0xC0));
        let box_marked = Paint::color(Color::rgb(0x40, 0xC0, 0x40));

        let button_fill = Paint::color(Color::black())
            .with_font_size(font_size)
            .with_text_baseline(Baseline::Top)
            .with_text_align(Align::Center)
            .with_line_width(1.0);

        let text_stroke = Paint::color(Color::rgba(0, 0, 0, 0))
            .with_font_size(font_size)
            .with_text_baseline(Baseline::Top)
            .with_line_width(1.0);
        let text_fill = text_stroke.clone().with_color(Color::white());

        let x = 0x10 as f32;
        let mut y = 0x10 as f32;
        let mut fill_text = |button, text, marked: bool| {
            let box_fill = marked.select(&box_marked, &box_fill);

            let mut path = Path::new();
            path.rounded_rect(x, y, font_size, font_size, 4.0);
            self.canvas.fill_path(&path, box_fill);
            self.canvas.stroke_path(&path, &box_stroke);

            let _ = self
                .canvas
                .fill_text(x + font_size / 2.0, y, button, &button_fill);

            let _ = self.canvas.fill_text(x + font_size, y, text, &text_fill);
            let _ = self
                .canvas
                .stroke_text(x + font_size, y, text, &text_stroke);

            y += font_size;
        };

        if self.show_help {
            fill_text("S", "Save", false);
            fill_text("O", "Open", false);
            fill_text("L", "Snapping", editor.snapping());
            fill_text("E", "Text editor", editor.text_editor_shown());
            fill_text("F", "Search", editor.search_mode());
            fill_text("N", "New", false);
            fill_text("P", "Simulation mode", editor.simulation_mode());
            fill_text("A", "Add state", false);
            fill_text("C", "Copy", false);
            fill_text("V", "Paste", false);
            fill_text("G", "Create group", false);
            fill_text(
                "1",
                "Default view mode",
                editor.view_mode() == ViewMode::Default,
            );
            fill_text(
                "2",
                "State view mode",
                editor.view_mode() == ViewMode::State,
            );
            fill_text(
                "3",
                "Action view mode",
                editor.view_mode() == ViewMode::Actions,
            );
            fill_text("H", "Hide help", true);
        } else {
            fill_text("H", "Show help", false);
        }

        self.canvas.flush();
    }

    fn draw_place(
        &mut self,
        [x, y]: [f32; 2],
        radius: f32,
        zoom: f32,
        name: &TextLine,
        cursor: Option<usize>,
        count: usize,
        marked: bool,
    ) {
        let x = x / zoom;
        let y = y / zoom;
        let r = radius / zoom;

        let mut path = Path::new();
        let (start_color, end_color) = if count == 0 {
            (Color::rgb(0xC0, 0x0, 0x0), Color::rgb(0xC0, 0x40, 0x80))
        } else {
            (Color::rgb(0x0, 0x40, 0xC0), Color::rgb(0x0, 0x80, 0x40))
        };
        path.circle(x, y, r);
        let fill_paint = Paint::radial_gradient(x, y, r * 0.5, r, start_color, end_color);
        self.canvas.fill_path(&path, &fill_paint);
        let (stroke_color, stroke_width) = if marked {
            (Color::rgb(0xC0, 0xC0, 0), 4.0)
        } else {
            (Color::black(), 2.0)
        };
        let stroke_paint = Paint::color(stroke_color).with_line_width(stroke_width);
        self.canvas.stroke_path(&path, &stroke_paint);

        let text_paint = Paint::color(Color::white())
            .with_font(&[self.font])
            .with_font_size(self.text_size / zoom)
            .with_text_align(Align::Center)
            .with_text_baseline(Baseline::Middle);

        let _ = self.canvas.fill_text(x, y, format!("{count}"), &text_paint);

        let text_paint = text_paint.with_text_baseline(Baseline::Top);

        render_text(
            name.as_str(),
            x,
            y + self.text_size / zoom,
            self.text_width / zoom,
            self.text_size / zoom,
            &text_paint,
            cursor.map(|cursor| CursorSettings {
                width: 1.0 / zoom,
                index: name.string_index(cursor),
            }),
            cursor.map(|_| 3),
            &mut self.canvas,
        );
    }

    fn draw_transition(
        &mut self,
        [x, y]: [f32; 2],
        width: f32,
        height: f32,
        zoom: f32,
        name: &TextLine,
        cursor: Option<usize>,
        marked: bool,
        callable: CallableState,
    ) {
        let x = x / zoom;
        let y = y / zoom;

        let mut path = Path::new();
        let (start_color, end_color) = match (callable.forward, callable.backward) {
            (true, true) => (Color::rgb(0x40, 0xC0, 0x40), Color::rgb(0x20, 0x60, 0x20)),
            (true, false) => (Color::rgb(0xC0, 0xC0, 0x20), Color::rgb(0x60, 0x40, 0x20)),
            (false, true) => (Color::rgb(0x80, 0x80, 0xC0), Color::rgb(0x20, 0x40, 0x60)),
            (false, false) => (Color::rgb(0xC0, 0xE0, 0xE0), Color::rgb(0x80, 0x40, 0x40)),
        };
        let w = width / zoom;
        let h = height / zoom;
        let xmin = x - w / 2.0;
        let ymin = y - h / 2.0;

        path.rounded_rect(xmin, ymin, width / zoom, height / zoom, 8.0 / zoom);
        let fill_paint = Paint::box_gradient(
            xmin + 4.0 / zoom,
            ymin + 4.0 / zoom,
            w - 8.0 / zoom,
            h - 8.0 / zoom,
            4.0 / zoom,
            8.0 / zoom,
            start_color,
            end_color,
        );
        self.canvas.fill_path(&path, &fill_paint);
        let (stroke_color, stroke_width) = if marked {
            (Color::rgb(0xC0, 0xC0, 0), 4.0)
        } else {
            (Color::black(), 2.0)
        };
        let stroke_paint = Paint::color(stroke_color).with_line_width(stroke_width);
        self.canvas.stroke_path(&path, &stroke_paint);

        let text_paint = Paint::color(Color::black())
            .with_font(&[self.font])
            .with_font_size(self.text_size / zoom)
            .with_text_align(Align::Center)
            .with_text_baseline(Baseline::Middle);

        render_text(
            name.as_str(),
            x,
            y,
            self.text_width / zoom,
            self.text_size / zoom,
            &text_paint,
            cursor.map(|cursor| CursorSettings {
                width: 1.0 / zoom,
                index: name.string_index(cursor),
            }),
            cursor.map(|_| 3),
            &mut self.canvas,
        );
    }

    fn draw_transition_frame(
        &mut self,
        left: f32,
        right: f32,
        top: f32,
        bottom: f32,
        zoom: f32,
        name: &str,
    ) {
        let left = left / zoom;
        let right = right / zoom;
        let top = top / zoom;
        let bottom = bottom / zoom;
        let width = right - left;
        let height = bottom - top;

        let mut path = Path::new();
        path.rounded_rect(left, top, width, height, 8.0 / zoom);
        let fill_paint = Paint::color(Color::rgba(0xFF, 0xFF, 0xFF, 0x10));

        self.canvas.fill_path(&path, &fill_paint);
        let stroke_paint = Paint::color(Color::black()).with_line_width(2.0);
        self.canvas.stroke_path(&path, &stroke_paint);

        let text_paint = Paint::color(Color::black())
            .with_font(&[self.font])
            .with_font_size(self.text_size / zoom)
            .with_text_align(Align::Center)
            .with_text_baseline(Baseline::Top);

        render_text(
            name,
            (left + right) / 2.0,
            top,
            self.text_width / zoom,
            self.text_size / zoom,
            &text_paint,
            None,
            None,
            &mut self.canvas,
        );
    }

    fn draw_line(&mut self, [from_x, from_y]: [f32; 2], [to_x, to_y]: [f32; 2], zoom: f32) {
        let from_x = from_x / zoom;
        let from_y = from_y / zoom;
        let to_x = to_x / zoom;
        let to_y = to_y / zoom;
        let mut path = Path::new();
        path.move_to(from_x, from_y);
        path.line_to(to_x, to_y);
        let paint = Paint::color(Color::rgbaf(1.0, 0.5, 0.5, 0.5)).with_line_width(2.0);
        self.canvas.stroke_path(&path, &paint);
    }

    fn draw_arrow(
        &mut self,
        [from_x, from_y]: [f32; 2],
        [to_x, to_y]: [f32; 2],
        radius: f32,
        width: f32,
        height: f32,
        zoom: f32,
        arrow_kind: ArrowKind,
    ) {
        use ArrowKind::*;
        let (from_point, to_point) = match arrow_kind {
            PointToPlace | PointToTransition => (true, false),
            PlaceToPoint | TransitionToPoint => (false, true),
            PlaceToTransition | TransitionToPlace => (false, false),
        };

        let from = Vector::new(from_x, from_y) / zoom;
        let to = Vector::new(to_x, to_y) / zoom;
        let dv = to - from;
        let poffset = dv.normalize() * radius / zoom;
        let zoom2 = zoom * 2.0;

        use RenderFlow::*;
        let (toffset, tdirection) = match self.flow {
            OmniDirectional => {
                if dv.x.abs().mul_add(zoom2, height) > dv.y.abs().mul_add(zoom2, width) {
                    let y = dv.y / (dv.x.abs() + (height - width) / zoom2);
                    (
                        Vector::new(if dv.x > 0.0 { width } else { -width }, y * height) / zoom2,
                        Vector::new(if dv.x > 0.0 { 1.0 } else { -1.0 }, y * y * y).normalize(),
                    )
                } else {
                    let x = dv.x / (dv.y.abs() + (width - height) / zoom2);
                    (
                        Vector::new(x * width, if dv.y > 0.0 { height } else { -height }) / zoom2,
                        Vector::new(x * x * x, if dv.y > 0.0 { 1.0 } else { -1.0 }).normalize(),
                    )
                }
            }
            Horizontal => {
                let y = (dv.y / height * zoom2 / dv.x.cbrt().max(1.0)).atan() * 2.0
                    / std::f32::consts::PI;
                (Vector::new(width, y * height) / zoom2, Vector::x(1.0))
            }
            Vertical => {
                let x = (dv.x / width * zoom2 / dv.y.cbrt().max(1.0)).atan() * 2.0
                    / std::f32::consts::PI;
                (Vector::new(x * width, height) / zoom2, Vector::y(1.0))
            }
        };
        let (from_offset, from_direction, to_offset, to_direction) = match arrow_kind {
            PlaceToPoint | PlaceToTransition | PointToTransition => {
                (poffset, poffset.normalize(), toffset, tdirection)
            }
            PointToPlace | TransitionToPlace | TransitionToPoint => {
                (toffset, tdirection, poffset, poffset.normalize())
            }
        };

        let from = if from_point { from } else { from + from_offset };
        let to = if to_point { to } else { to - to_offset };
        let mag = (to - from).magnitude();
        let from2 = from + from_direction * mag / 3.0;
        let to2 = to - to_direction * mag / 3.0;

        let last_point = match (from_point, to_point) {
            (true, true) => from,
            (false, true) => from2,
            (_, false) => to2,
        };

        let dir = (to - last_point).normalize();
        let tip = to;
        let to = tip - dir * 8.0;

        let mut path = Path::new();
        path.move_to(from.x, from.y);
        match (from_point, to_point) {
            (true, true) => path.line_to(to.x, to.y),
            (true, false) => path.quad_to(to2.x, to2.y, to.x, to.y),
            (false, true) => path.quad_to(from2.x, from2.y, to.x, to.y),
            (false, false) => path.bezier_to(from2.x, from2.y, to2.x, to2.y, to.x, to.y),
        }
        let paint = Paint::color(Color::black()).with_line_width(4.0);
        self.canvas.stroke_path(&path, &paint);

        let mut path = Path::new();
        let orth = Vector::new(dir.y, -dir.x);
        let left = tip - dir * 16.0 + orth * 8.0;
        let right = tip - dir * 16.0 - orth * 8.0;

        path.move_to(tip.x, tip.y);
        path.line_to(left.x, left.y);
        path.line_to(right.x, right.y);
        path.line_to(tip.x, tip.y);
        self.canvas.fill_path(&path, &paint);
    }

    fn draw_field(&mut self, text: &TextLine, text_cursor: usize) {
        let w = self.canvas.width() as f32;
        let h = self.canvas.height() as f32;

        let xmin = 32.0;
        let xmax = w - 32.0;
        let ymin = h - 64.0;
        let ymax = h - 32.0;

        let mut path = Path::new();
        path.rounded_rect(xmin, ymin, xmax - xmin, ymax - ymin, 16.0);

        self.canvas.fill_path(
            &path,
            &Paint::linear_gradient(
                xmin,
                ymin,
                xmax,
                ymax,
                Color::rgb(0x80, 0x80, 0x80),
                Color::rgb(0x40, 0xFF, 0xC0),
            ),
        );
        let stroke_paint = Paint::color(Color::black()).with_line_width(2.0);
        self.canvas.stroke_path(&path, &stroke_paint);

        let text_paint = Paint::color(Color::black())
            .with_font(&[self.font])
            .with_font_size(24.0)
            .with_text_align(Align::Left)
            .with_text_baseline(Baseline::Middle);

        let (x, y) = (xmin + 16.0, (ymax + ymin) / 2.0);
        let _ = self.canvas.fill_text(x, y, text.as_str(), &text_paint);
        self.draw_cursor(
            x,
            y,
            1.0,
            text.as_str(),
            text.string_index(text_cursor),
            &text_paint,
        );
    }

    fn draw_select_box(&mut self, [x1, y1]: [f32; 2], [x2, y2]: [f32; 2], zoom: f32) {
        let mut path = Path::new();
        path.rect(x1 / zoom, y1 / zoom, (x2 - x1) / zoom, (y2 - y1) / zoom);

        let paint = Paint::color(Color::rgbaf(0.0, 0.0, 0.0, 0.5)).with_line_width(2.0);

        self.canvas.stroke_path(&path, &paint);
    }

    fn draw_text_box(
        &mut self,
        text: &[TextLine],
        text_cursor: usize,
        text_line: usize,
        text_offset: usize,
    ) {
        let mut path = Path::new();
        let w = self.canvas.width() as f32;
        let h = self.canvas.height() as f32;
        path.rounded_rect(16.0 + w / 2.0, 8.0, w / 2.0 - 32.0, h - 32.0, 32.0);
        self.canvas
            .fill_path(&path, &Paint::color(Color::rgba(0xFF, 0xFF, 0xFF, 0xc0)));

        let stroke_paint = Paint::color(Color::rgba(0, 0, 0, 0x80)).with_line_width(4.0);
        self.canvas.stroke_path(&path, &stroke_paint);

        let text_paint = Paint::color(Color::black())
            .with_font(&[self.font])
            .with_font_size(16.0)
            .with_text_align(Align::Left)
            .with_text_baseline(Baseline::Top);

        let w = self.canvas.width() as f32;
        let (x, y) = (32.0 + w / 2.0, 32.0);
        let w = w / 2.0 - 64.0;

        let line_count =
            ((self.canvas.height() as f32 / self.text_size * 4.0) as usize).saturating_sub(5);

        let mut current_line = 0;
        for (i, line) in text[text_offset..].iter().enumerate() {
            if current_line >= line_count {
                break;
            }
            current_line += render_text(
                line.as_str(),
                x,
                self.text_size.mul_add(current_line as f32, y),
                w,
                self.text_size,
                &text_paint,
                (text_line == i).then(|| CursorSettings {
                    width: 1.0,
                    index: line.string_index(text_cursor),
                }),
                Some(line_count - current_line),
                &mut self.canvas,
            )
            .max(1);
        }
    }
}
