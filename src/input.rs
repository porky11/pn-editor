use crate::renderer::{NetRenderer, RenderFlow};

use bool_utils::BoolUtils;
use femtovg::Renderer;
use ga2::Vector;
use gapp_winit::WindowInput;
use pn_editor_core::{
    ActionButton, CursorBoundary, Editor, FireDirection, LineDirection, LineScope, ViewMode,
};
use text_editing::Direction;
use vector_space::distance;
use winit::{
    event::{
        ElementState, KeyboardInput, ModifiersState, MouseButton, Touch, TouchPhase,
        VirtualKeyCode, WindowEvent,
    },
    event_loop::ControlFlow,
};

#[derive(Default)]
struct TouchInfo {
    main: Option<u64>,
    zoomer: Option<u64>,
    distance: f32,
}

pub struct Context {
    modifiers: ModifiersState,
    touch: TouchInfo,
    cursor: Vector<f32>,
}

impl Default for Context {
    fn default() -> Self {
        Self {
            modifiers: ModifiersState::empty(),
            touch: TouchInfo::default(),
            cursor: Vector::new(0.0, 0.0),
        }
    }
}

impl<R: Renderer> WindowInput<Context, NetRenderer<R>> for Editor {
    fn input(
        &mut self,
        event: &WindowEvent<'_>,
        control_flow: &mut ControlFlow,
        context: &mut Context,
        renderer: &mut NetRenderer<R>,
    ) {
        match event {
            &WindowEvent::ModifiersChanged(modifiers) => context.modifiers = modifiers,

            WindowEvent::Resized(physical_size) => {
                let (w, h) = (physical_size.width, physical_size.height);
                renderer.canvas.set_size(w, h, 1.0);
                self.resize(w, h);
            }

            WindowEvent::CursorMoved { position, .. } => {
                self.set_cursor(position.x as f32, position.y as f32);
            }

            WindowEvent::MouseWheel {
                delta: winit::event::MouseScrollDelta::LineDelta(_, roll),
                ..
            } => self.zoom(
                (roll / -0x10 as f32).exp2(),
                !context.modifiers.shift(),
                context.modifiers.ctrl(),
            ),
            WindowEvent::MouseInput { button, state, .. } => {
                use MouseButton::*;
                let primary = match button {
                    Left => true,
                    Right => false,
                    _ => return,
                };
                use ElementState::*;
                match state {
                    Pressed => {
                        if context.modifiers.alt() {
                            let dir =
                                primary.select(FireDirection::Forward, FireDirection::Backward);
                            self.fire_node(dir);
                        } else {
                            let button = primary.select(ActionButton::Move, ActionButton::Special);
                            self.grab(button, !context.modifiers.shift(), context.modifiers.ctrl());
                        }
                    }
                    Released => self.release(
                        primary.select(ActionButton::Move, ActionButton::Special),
                        context.modifiers.ctrl(),
                        !context.modifiers.shift(),
                        !context.modifiers.alt(),
                    ),
                }
            }
            &WindowEvent::Touch(Touch {
                phase,
                location,
                id,
                ..
            }) => {
                let location = Vector::new(location.x as f32, location.y as f32);
                if let Some(current) = context.touch.main {
                    if current == id {
                        context.cursor = location;
                        self.set_cursor(location.x, location.y);

                        if phase == TouchPhase::Ended {
                            self.release(
                                ActionButton::Move,
                                context.modifiers.ctrl(),
                                !context.modifiers.shift(),
                                !context.modifiers.alt(),
                            );
                            context.touch.main = None;
                        }
                    }
                } else if phase == TouchPhase::Started {
                    if context.modifiers.alt() {
                        self.fire_node(FireDirection::Forward);
                    } else {
                        self.grab(
                            ActionButton::Move,
                            !context.modifiers.shift(),
                            context.modifiers.ctrl(),
                        );
                    }
                    context.cursor = location;
                    self.set_cursor(location.x, location.y);
                    context.touch.main = Some(id);
                }

                let dis = distance(location, context.cursor);

                if dis == 0.0 {
                    return;
                }

                let Some(zoomer) = context.touch.zoomer else {
                    if phase == TouchPhase::Started {
                        context.touch.distance = dis;
                        context.touch.zoomer = Some(id);
                    }

                    return;
                };

                if zoomer == id {
                    self.zoom(
                        context.touch.distance / dis,
                        !context.modifiers.shift(),
                        context.modifiers.ctrl(),
                    );
                    context.touch.distance = dis;

                    if phase == TouchPhase::Ended {
                        context.touch.zoomer = None;
                    }
                }
            }

            WindowEvent::ReceivedCharacter(c) => {
                if !context.modifiers.ctrl() && !context.modifiers.alt() {
                    self.text_input(*c, context.modifiers.ctrl());
                }
            }
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        virtual_keycode: Some(keycode),
                        state,
                        ..
                    },
                ..
            } => {
                if *state != ElementState::Pressed {
                    return;
                }

                use VirtualKeyCode::*;
                match keycode {
                    Escape => self.escape(),
                    Return => self.finish(!context.modifiers.shift(), context.modifiers.ctrl()),
                    Left => self.move_text_cursor(Direction::Backward, context.modifiers.ctrl()),
                    Right => self.move_text_cursor(Direction::Forward, context.modifiers.ctrl()),
                    Up => self.move_line(LineDirection::Up),
                    Down => self.move_line(LineDirection::Down),
                    PageUp => self.move_page(LineDirection::Up),
                    PageDown => self.move_page(LineDirection::Down),
                    Home => self.move_text_cursor_border(
                        CursorBoundary::Start,
                        context
                            .modifiers
                            .ctrl()
                            .select(LineScope::AllLines, LineScope::CurrentLine),
                    ),
                    End => self.move_text_cursor_border(
                        CursorBoundary::End,
                        context
                            .modifiers
                            .ctrl()
                            .select(LineScope::AllLines, LineScope::CurrentLine),
                    ),
                    Tab => self.cycle(context.modifiers.ctrl()),
                    Delete if context.modifiers.alt() => self.remove_nodes(),
                    code if context.modifiers.alt() => match code {
                        S => self.save_progress(context.modifiers.shift()),
                        O => self.load_progress(),
                        G => self.ungroup_nodes(context.modifiers.shift()),
                        Key1 => renderer.set_flow(RenderFlow::OmniDirectional),
                        Key2 => renderer.set_flow(RenderFlow::Horizontal),
                        Key3 => renderer.set_flow(RenderFlow::Vertical),
                        _ => (),
                    },
                    code if context.modifiers.ctrl() => match code {
                        NumpadAdd | Plus => self.start_nodes(),
                        S => self.save(context.modifiers.shift()),
                        O => self.load(!context.modifiers.shift(), false),
                        L => self.toggle_snapping(),
                        E => self.toggle_editor(),
                        F => self.start_search_mode(),
                        N => self.load_new(false),
                        P => self.switch_mode(false),
                        A => self.add_state(),
                        C => self.copy_selected(),
                        X => self.cut_selected(),
                        V => self.paste(!context.modifiers.shift()),
                        G => self.group_nodes(!context.modifiers.shift()),
                        Key1 => self.set_view_mode(ViewMode::Default),
                        Key2 => self.set_view_mode(ViewMode::State),
                        Key3 => self.set_view_mode(ViewMode::Actions),
                        H => renderer.toggle_help(),
                        _ => (),
                    },
                    _ => (),
                }
            }
            WindowEvent::CloseRequested => {
                if self.ensure_saved(false) {
                    *control_flow = ControlFlow::Exit;
                }
            }
            _ => (),
        }
    }
}
